# A minimal configuration.nix containing just Arcan.
{pkgs,...}:{
  environment.systemPackages = let
    arcan = (pkgs.callPackage (import ./Arcan.nix) {});
  in [
    arcan.arcan

    arcan.acfgfs
    arcan.aloadimage
    arcan.aclip
    arcan.shmmon

    arcan.prio
    arcan.durden
  ];
  users.groups.input.members = ["user"];
}